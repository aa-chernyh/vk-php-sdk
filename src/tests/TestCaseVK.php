<?php

namespace VKSDKTests;

use PHPUnit\Framework\TestCase;
use VKSDK\client\AuthClient;
use VKSDK\client\AuthConfig;
use VKSDK\client\Client;
use VKSDK\client\Config;
use VKSDK\exception\AuthException;
use VKSDK\request\AudioGetRequest;
use VKSDK\request\AudioSearchRequest;
use VKSDK\request\GetPopularRequest;
use VKSDK\request\GetRecommendationsRequest;
use VKSDK\request\UsersGetRequest;

class TestCaseVK extends TestCase
{

    const LOGIN = '';
    const PASSWORD = '';

    /**
     * @var Config|null
     */
    private $config;

    public function testGetAudioList(): void
    {
        $client = new Client($this->getClientConfig());

        $request = new AudioGetRequest();
        $response = $client->getAudioList($request);

        $this->assertTrue($response->isSuccess() && count($response->response->items));
    }

    public function testSearchAudio(): void
    {
        $client = new Client($this->getClientConfig());

        $request = new AudioSearchRequest();
        $request->q = 'DJ Amor';
        $response = $client->searchAudio($request);

        $this->assertTrue($response->isSuccess() && count($response->response->items));
    }

    public function testGetRecommendations(): void
    {
        $client = new Client($this->getClientConfig());

        $request = new GetRecommendationsRequest();
        $request->user_id = '146590181';
        $response = $client->getRecommendations($request);

        $this->assertTrue($response->isSuccess() && count($response->response->items));
    }

    public function testGetPopular(): void
    {
        $client = new Client($this->getClientConfig());

        $request = new GetPopularRequest();
        $response = $client->getPopular($request);

        $this->assertTrue($response->isSuccess() && count($response->response));
    }

    public function testGetUserLIst(): void
    {
        $client = new CLient($this->getClientConfig());

        $request = new UsersGetRequest();
        $request->user_ids = 'k.kolodina';
        $response = $client->getUserList($request);

        $this->assertTrue(
            $response->isSuccess()
            && count($response->response)
            && $response->response[0]->id == 139096729
        );
    }

    /**
     * @return Config
     * @throws AuthException
     */
    private function getClientConfig(): Config
    {
        if(!$this->config) {
            $authConfig = new AuthConfig(self::LOGIN, self::PASSWORD);
            $authClient = new AuthClient($authConfig);
            $this->config = $authClient->getClientConfig();
        }

        return $this->config;
    }
}