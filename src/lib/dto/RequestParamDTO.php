<?php

namespace VKSDK\dto;

class RequestParamDTO
{

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $value;
}