<?php

namespace VKSDK\dto;

class ArtistDTO
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $domain;

    /**
     * @var int
     */
    public $id;
}