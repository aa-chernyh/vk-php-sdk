<?php

namespace VKSDK\dto;

class ErrorDTO
{

    /**
     * @var integer
     */
    public $error_code;

    /**
     * @var string
     */
    public $error_msg;

    /**
     * @var RequestParamDTO[]
     */
    public $request_params;
}