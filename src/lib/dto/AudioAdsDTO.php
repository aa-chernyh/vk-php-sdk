<?php

namespace VKSDK\dto;

class AudioAdsDTO
{

    /**
     * @var string
     */
    public $content_id;

    /**
     * Длительность в секундах
     * @var int
     */
    public $duration;

    /**
     * @var int
     */
    public $account_age_type;

    /**
     * @var int
     */
    public $puid1;

    /**
     * @var int
     */
    public $puid22;
}