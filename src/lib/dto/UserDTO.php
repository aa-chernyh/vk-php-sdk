<?php


namespace VKSDK\dto;


class UserDTO
{

    /**
     * Имя
     * @var string
     */
    public $first_name;

    /**
     * ID
     * @var int
     */
    public $id;

    /**
     * Фамилия
     * @var string
     */
    public $last_name;

    /**
     * Есть ли у текущего пользователя возможность видеть профиль пользователя при is_closed = true
     * @var boolean
     */
    public $can_access_closed;

    /**
     * Включена ли приватность профиля
     * @var boolean
     */
    public $is_closed;
}