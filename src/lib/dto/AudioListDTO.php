<?php

namespace VKSDK\dto;

class AudioListDTO
{

    /**
     * @var int
     */
    public $count;

    /**
     * @var AudioDTO[]
     */
    public $items;
}