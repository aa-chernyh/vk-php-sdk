<?php

namespace VKSDK\dto;

use VKSDK\enum\Genre;

class AudioDTO
{

    /**
     * Автор
     * @var string
     */
    public $artist;

    /**
     * @var int
     */
    public $id;

    /**
     * ID владельца (у кого добавлена запись)
     * @var int
     */
    public $owner_id;

    /**
     * Название
     * @var string
     */
    public $title;

    /**
     * Длительность в секундах
     * @var string
     */
    public $duration;

    /**
     * @var string
     */
    public $access_key;

    /**
     * @var AudioAdsDTO
     */
    public $ads;

    /**
     * @var bool
     */
    public $is_explicit;

    /**
     * @var bool
     */
    public $is_licensed;

    /**
     * @var string
     */
    public $track_code;

    /**
     * Ссылка на скачивание
     * @var string
     */
    public $url;

    /**
     * Дата добавления в аудиозаписи
     * @var int
     */
    public $date;

    /**
     * ID текста
     * @var int|null
     */
    public $lyrics_id;

    /**
     * Не предлагать в поиске
     * @var int|null
     */
    public $no_search;

    /**
     * ID жанра
     * @var int
     */
    public $genre_id;

    /**
     * High Quality
     * @var bool
     */
    public $is_hq;

    /**
     * Ограниченный контент
     * @var int|null
     */
    public $content_restricted;

    /**
     * Артисты
     * @var ArtistDTO[]|null
     */
    public $main_artists;

    /**
     * Избранные артисты
     * @var ArtistDTO[]|null
     */
    public $featured_artists;

    /**
     * Описание
     * @var string|null
     */
    public $subtitle;

    /**
     * @var bool
     */
    public $short_videos_allowed;

    /**
     * @var bool
     */
    public $stories_allowed;

    /**
     * @var bool
     */
    public $stories_cover_allowed;

    public function getGenreName(): ?string
    {
        if($this->genre_id && isset(Genre::MAPPING[$this->genre_id])) {
            return Genre::MAPPING[$this->genre_id];
        }

        return null;
    }
}