<?php

namespace VKSDK\exception;

class InvalidConfigException extends VKException
{
    public const EMPTY_DATA_ERROR_MESSAGE = 'Пустые данные авторизации';
}