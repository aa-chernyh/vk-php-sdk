<?php

namespace VKSDK\enum;

class Genre
{
    public const ROCK = 1;
    public const POP = 2;
    public const RAP_AND_HIP_HOP = 3;
    public const EASY_LISTENING = 4;
    public const DANCE_AND_HOUSE = 5;
    public const INSTRUMENTAL = 6;
    public const METAL = 7;
    public const DUBSTEP = 8;
    public const JAZZ_AND_BLUES = 9;
    public const DRUM_AND_BASS = 10;
    public const TRANCE = 11;
    public const CHANSON = 12;
    public const ETHNIC = 13;
    public const ACOUSTIC_AND_VOCAL = 14;
    public const REGGAE = 15;
    public const CLASSICAL = 16;
    public const INDIE_POP = 17;
    public const OTHER = 18;
    public const SPEECH = 19;
    public const ALTERNATIVE = 21;
    public const ELECTROPOP_AND_DISCO = 22;
    public const UNDEFINED_1 = 250;
    public const UNDEFINED_2 = 1001;

    public const NAME_ROCK = "Rock";
    public const NAME_POP = "Pop";
    public const NAME_RAP_AND_HIP_HOP = "Rap & Hip-Hop";
    public const NAME_EASY_LISTENING = "Easy Listening";
    public const NAME_DANCE_AND_HOUSE = "Dance & House";
    public const NAME_INSTRUMENTAL = "Instrumental";
    public const NAME_METAL = "Metal";
    public const NAME_DUBSTEP = "Dubstep";
    public const NAME_JAZZ_AND_BLUES = "Jazz & Blues";
    public const NAME_DRUM_AND_BASS = "Drum & Bass";
    public const NAME_TRANCE = "Trance";
    public const NAME_CHANSON = "Chanson";
    public const NAME_ETHNIC = "Ethnic";
    public const NAME_ACOUSTIC_AND_VOCAL = "Acoustic & Vocal";
    public const NAME_REGGAE = "Reggae";
    public const NAME_CLASSICAL = "Classical";
    public const NAME_INDIE_POP = "Indie Pop";
    public const NAME_OTHER = "Other";
    public const NAME_SPEECH = "Speech";
    public const NAME_ALTERNATIVE = "Alternative";
    public const NAME_ELECTROPOP_AND_DISCO = "Electropop & Disco";
    public const NAME_UNDEFINED_1 = 'Неизвестный 1';
    public const NAME_UNDEFINED_2 = 'Неизвестный 2';

    public const MAPPING = [
        self::ROCK => self::NAME_ROCK,
        self::POP => self::NAME_POP,
        self::RAP_AND_HIP_HOP => self::NAME_RAP_AND_HIP_HOP,
        self::EASY_LISTENING => self::NAME_EASY_LISTENING,
        self::DANCE_AND_HOUSE => self::NAME_DANCE_AND_HOUSE,
        self::INSTRUMENTAL => self::NAME_INSTRUMENTAL,
        self::METAL => self::NAME_METAL,
        self::DUBSTEP => self::NAME_DUBSTEP,
        self::JAZZ_AND_BLUES => self::NAME_JAZZ_AND_BLUES,
        self::DRUM_AND_BASS => self::NAME_DRUM_AND_BASS,
        self::TRANCE => self::NAME_TRANCE,
        self::CHANSON => self::NAME_CHANSON,
        self::ETHNIC => self::NAME_ETHNIC,
        self::ACOUSTIC_AND_VOCAL => self::NAME_ACOUSTIC_AND_VOCAL,
        self::REGGAE => self::NAME_REGGAE,
        self::CLASSICAL => self::NAME_CLASSICAL,
        self::INDIE_POP => self::NAME_INDIE_POP,
        self::OTHER => self::NAME_OTHER,
        self::SPEECH => self::NAME_SPEECH,
        self::ALTERNATIVE => self::NAME_ALTERNATIVE,
        self::ELECTROPOP_AND_DISCO => self::NAME_ELECTROPOP_AND_DISCO,
        self::UNDEFINED_1 => self::NAME_UNDEFINED_1,
        self::UNDEFINED_2 => self::NAME_UNDEFINED_2,
    ];
}