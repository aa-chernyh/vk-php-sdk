<?php

namespace VKSDK\response;

use VKSDK\dto\AudioListDTO;
use VKSDK\response\base\BaseResponse;

class AudioListResponse extends BaseResponse
{

    /**
     * @var AudioListDTO
     */
    public $response;
}