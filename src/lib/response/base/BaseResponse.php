<?php

namespace VKSDK\response\base;

use ClientInterface\Base\PhpDocReader\AnnotationException;
use ClientInterface\Base\StructureHelper;
use ReflectionException;
use VKSDK\dto\ErrorDTO;

class BaseResponse extends AbstractResponse
{

    /**
     * @var ErrorDTO|null
     */
    public $error = [];

    public function isSuccess(): bool
    {
        return !$this->error;
    }

    /**
     * @return string[]
     */
    public function getErrors(): array
    {
        if($this->error->error_msg) {
            return [$this->error->error_msg];
        }

        return [];
    }

    /**
     * @param array $rawData
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public function fill(array $rawData): void
    {
        StructureHelper::fill($this, $rawData, false);
    }
}