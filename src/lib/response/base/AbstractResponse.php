<?php

namespace VKSDK\response\base;

use ClientInterface\Response;

abstract class AbstractResponse implements Response
{
    abstract public function fill(array $rawData): void;
}