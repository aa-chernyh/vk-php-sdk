<?php

namespace VKSDK\response;

use VKSDK\dto\UserDTO;
use VKSDK\response\base\BaseResponse;

class UserListResponse extends BaseResponse
{

    /**
     * @var UserDTO[]
     */
    public $response;
}