<?php

namespace VKSDK\response;

use VKSDK\dto\AudioDTO;
use VKSDK\response\base\BaseResponse;

class GetPopularResponse extends BaseResponse
{

    /**
     * @var AudioDTO[]
     */
    public $response;
}