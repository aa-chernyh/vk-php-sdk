<?php

namespace VKSDK\client;

use VKSDK\exception\AuthException;
use VKSDK\exception\InvalidConfigException;
use Vodka2\VKAudioToken\TokenException;
use Vodka2\VKAudioToken\TokenFacade;

class AuthClient extends AbstractClient
{

    /**
     * @var AuthConfig
     */
    private $config;

    public function __construct(AuthConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @return Config
     * @throws AuthException
     * @throws InvalidConfigException
     */
    public function getClientConfig(): Config
    {
        try {
            $result = TokenFacade::getKateToken($this->config->getLogin(), $this->config->getPassword());
            if(!isset($result['token']) || !isset($result['userAgent'])) {
                throw new AuthException('Не получен token либо userAgent');
            }
            return new Config($result['token'], $result['userAgent']);
        } catch (TokenException $exception) {
            throw new AuthException($this->getTokenExceptionRusMessage($exception));
        }
    }

    private function getTokenExceptionRusMessage(TokenException $exception): string
    {
        switch ($exception->code) {
            case TokenException::REGISTRATION_ERROR:
                return "Неверные данные авторизации" ;
            case TokenException::TOKEN_NOT_REFRESHED:
                return "Токен не обновлен и остался тем же";
            case TokenException::TOKEN_NOT_RECEIVED:
                return "Токен не получен ({$exception->getMessage()})";
            case TokenException::REQUEST_ERR:
                return "Ошибка во время выполнения запроса ({$exception->getMessage()})";
            case TokenException::TWOFA_REQ:
                return 'Необходима двухфакторная авторизация';
            case TokenException::TWOFA_ERR:
                return "Ошибка двухфакторной авторизации ({$exception->getMessage()})";
            case TokenException::REFRESH_ERROR:
                return "Ошибка обновления токена ({$exception->getMessage()})";
        }
    }
}