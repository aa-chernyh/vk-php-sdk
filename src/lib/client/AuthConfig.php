<?php

namespace VKSDK\client;

use VKSDK\exception\InvalidConfigException;

class AuthConfig
{
    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * AuthConfig constructor.
     * @param string $login
     * @param string $password
     * @throws InvalidConfigException
     */
    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;

        $this->checkData();
    }

    /**
     * @throws InvalidConfigException
     */
    private function checkData(): void
    {
        if(!$this->login || !$this->password) {
            throw new InvalidConfigException(InvalidConfigException::EMPTY_DATA_ERROR_MESSAGE);
        }
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}