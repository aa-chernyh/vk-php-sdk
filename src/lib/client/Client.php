<?php

namespace VKSDK\client;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\TransferException;
use VKSDK\request\AudioGetRequest;
use VKSDK\request\AudioSearchRequest;
use VKSDK\request\GetPopularRequest;
use VKSDK\request\GetRecommendationsRequest;
use VKSDK\request\UsersGetRequest;
use VKSDK\response\AudioListResponse;
use VKSDK\response\GetPopularResponse;
use VKSDK\response\UserListResponse;

class Client extends AbstractClient
{

    const METHOD_AUDIO_GET_LIST = '/method/audio.get/';
    const METHOD_AUDIO_SEARCH = '/method/audio.search/';
    const METHOD_GET_RECOMMENDATIONS = '/method/audio.getRecommendations/';
    const METHOD_GET_POPULAR = '/method/audio.getPopular/';
    const METHOD_USERS_GET = '/method/users.get';

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function getAudioList(AudioGetRequest $request): AudioListResponse
    {
        return $this->makeRequest(
            self::METHOD_AUDIO_GET_LIST,
            $request,
            new AudioListResponse()
        );
    }

    public function searchAudio(AudioSearchRequest $request): AudioListResponse
    {
        return $this->makeRequest(
            self::METHOD_AUDIO_SEARCH,
            $request,
            new AudioListResponse()
        );
    }

    public function getRecommendations(GetRecommendationsRequest $request): AudioListResponse
    {
        return $this->makeRequest(
            self::METHOD_GET_RECOMMENDATIONS,
            $request,
            new AudioListResponse()
        );
    }

    public function getPopular(GetPopularRequest $request): GetPopularResponse
    {
        return $this->makeRequest(
            self::METHOD_GET_POPULAR,
            $request,
            new GetPopularResponse()
        );
    }

    public function getUserList(UsersGetRequest $request): UserListResponse
    {
        return $this->makeRequest(
            self::METHOD_USERS_GET,
            $request,
            new UserListResponse()
        );
    }

    private function makeRequest(string $method, $request, $response)
    {
        $request->access_token = $this->config->getAccessToken();
        $this->lastRequest = $this->unsetNullParams($request->toArray());

        $this->lastUrl = self::BASE_URL . $method;
        $this->lastRequestHeaders = [
            'User-Agent' => $this->config->getUserAgent(),
        ];

        $httpClient = new HttpClient([
            'base_uri' => self::BASE_URL,
            'timeout' => self::HTTP_TIMEOUT,
            'headers' => $this->lastRequestHeaders,
        ]);

        try {
            $result = $httpClient->request(self::HTTP_METHOD_POST, $method, ['form_params' => $this->lastRequest]);
            if($result->getStatusCode() != self::HTTP_STATUS_SUCCESS) {
                throw new TransferException("Неверный статус ответа: {$result->getStatusCode()}");
            }

            $this->lastResponseHeaders = $result->getHeaders();
            $this->lastResponse = json_decode($result->getBody()->getContents(), true);
            $response->fill($this->lastResponse);

            if(!$response->isSuccess()) {
                $this->lastErrorMessage = $response->getErrors()[0];
            }

            return $response;
        } catch (GuzzleException $exception) {
            $this->lastErrorMessage = $exception->getMessage();
            return null;
        }
    }

    private function unsetNullParams(array $request): array
    {
        foreach ($request as $key => $value) {
            if(is_array($value)) {
                $request[$key] = $this->unsetNullParams($value);
            } elseif(is_null($value)) {
                unset($request[$key]);
            }
        }

        return $request;
    }
}