<?php

namespace VKSDK\client;

use VKSDK\exception\InvalidConfigException;

class Config
{

    /**
     * @var string
     */
    public $accessToken;

    /**
     * @var string
     */
    public $userAgent;

    /**
     * Config constructor.
     * @param string $accessToken
     * @param string $userAgent
     * @throws InvalidConfigException
     */
    public function __construct(string $accessToken, string $userAgent)
    {
        $this->accessToken = $accessToken;
        $this->userAgent = $userAgent;

        $this->checkData();
    }

    /**
     * @throws InvalidConfigException
     */
    private function checkData(): void
    {
        if(!$this->accessToken || !$this->userAgent) {
            throw new InvalidConfigException(InvalidConfigException::EMPTY_DATA_ERROR_MESSAGE);
        }
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function toString(): string
    {
        return json_encode($this);
    }

    /**
     * @param string|null $configString
     * @return static
     * @throws InvalidConfigException
     */
    public static function createFromString(?string $configString): ?self
    {
        if($configString) {
            $decodedConfig = json_decode($configString, true);
            if(isset($decodedConfig['accessToken']) && isset($decodedConfig['userAgent'])) {
                return new self($decodedConfig['accessToken'], $decodedConfig['userAgent']);
            }
        }

        return null;
    }
}