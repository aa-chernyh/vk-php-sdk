<?php

namespace VKSDK\client;

use ClientInterface\Client as ClientInterface;

abstract class AbstractClient implements ClientInterface
{
    protected const BASE_URL = 'https://api.vk.com';

    protected const HTTP_TIMEOUT = 60;
    protected const HTTP_STATUS_SUCCESS = 200;

    protected const HTTP_METHOD_GET = 'GET';
    protected const HTTP_METHOD_POST = 'POST';

    /**
     * @var array
     */
    protected $lastRequest = [];

    /**
     * @var array
     */
    protected $lastResponse = [];

    /**
     * @var string
     */
    protected $lastErrorMessage = '';

    protected $lastUrl;

    protected $lastRequestHeaders = [];

    protected $lastResponseHeaders = [];

    public function getLastRequest(): array
    {
        return $this->lastRequest ?? [];
    }

    public function getLastResponse(): array
    {
        return $this->lastResponse ?? [];
    }

    public function getLastResult(): bool
    {
        return !$this->lastErrorMessage;
    }

    public function getLastErrorMessage(): string
    {
        return $this->lastErrorMessage;
    }

    public function getLastUrl(): string
    {
        return $this->lastUrl ?? '';
    }

    public function getLastRequestHeaders(): array
    {
        return $this->lastRequestHeaders ?? [];
    }

    public function getLastResponseHeaders(): array
    {
        return $this->lastResponseHeaders ?? [];
    }
}