<?php

namespace VKSDK\request\base;

use ClientInterface\Base\StructureHelper;

class BaseRequest extends AbstractRequest
{

    private const DEFAULT_CLIENT_VERSION = 5.95;

    /**
     * @var string
     */
    public $access_token;

    /**
     * Версия клиента
     * @var float
     */
    public $v = self::DEFAULT_CLIENT_VERSION;

    public function toArray(): array
    {
        return StructureHelper::toArray($this);
    }
}