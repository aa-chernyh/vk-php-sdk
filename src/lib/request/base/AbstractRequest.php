<?php

namespace VKSDK\request\base;

use ClientInterface\Request;

abstract class AbstractRequest implements Request
{
    abstract public function toArray(): array;

    public function validate(): bool
    {
        return true;
    }

    public function getFirstErrors(): array
    {
        return [];
    }

    public function getErrors($attribute = null): array
    {
        return [];
    }
}