<?php

namespace VKSDK\request;

use VKSDK\request\base\BaseRequest;

class GetPopularRequest extends BaseRequest
{

    /**
     * 1 – возвращать только зарубежные аудиозаписи. 0 – возвращать все аудиозаписи. (по умолчанию)
     * @var int
     */
    public $only_eng;

    /**
     * Идентификатор жанра из списка жанров.
     * Например, VKSDK\enum\Genre::ROCK
     * @var int
     */
    public $genre_id;

    /**
     * Смещение, необходимое для выборки определенного подмножества аудиозаписей.
     * @var int
     */
    public $offset;

    /**
     * Количество возвращаемых аудиозаписей.
     * Максимальное значение - 1000, по умолчанию - 100
     * @var int
     */
    public $count = 1000;
}